#!/usr/bin/env python

# This file is part of FS22_Vehicle_Years.
#
# FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# FS22_Vehicle_Years is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

from enum import StrEnum, auto
from pathlib import Path
from typing import Final
from multiprocessing import Pool

import argparse
import xml.etree.ElementTree as ET
import urllib.request
import re
import zipfile
import shutil
import random

YEAR_TBD: Final[str] = "TBD"

CDATA_START_MARKER: Final[str] = "$CDATA_START_MARKER"
CDATA_END_MARKER: Final[str] = "$CDATA_END_MARKER"
CDATA_START_REPLACE: Final[str] = "<![CDATA["
CDATA_END_REPLACE: Final[str] = "]]>"
XML_AMP: Final[str] = "&amp;"
XML_AMP_REPLACE: Final[str] = "&"
MIN_RATING_CNT: Final[int] = 100


class Category:
    """
    Class to hold information about a category

    Keyword arguments:
    category_name (str) -- The category with correct case as per the game
    max_per_year (int) -- The max_per_year of the category when genering decade lists.
                      This is effectively the average maximum number of vehicles per year,
                      and thus a value a 1 means twice as common as 0.5 and half as common as 2.

    """

    def __init__(
        self,
        category_name: str,
        max_per_year: float,
    ):
        self.category_name = category_name
        self.category_name_lower = category_name.lower()
        self.max_per_year = max_per_year


CATEGORIES: Final[Category] = [
    Category("animalTransport", 0.25),
    Category("animals", 0.25),
    Category("animalsVehicles", 0.5),
    Category("augerWagons", 0.5),
    Category("baleLoaders", 0.5),
    Category("baleWrappers", 0.5),
    Category("balers", 0.5),
    Category("balingVehicles", 0.5),
    Category("beetHarvesting", 0.5),
    Category("beetVehicles", 1),
    Category("belts", 1),
    Category("cars", 0.5),
    Category("cornHeaders", 0.25),
    Category("cottonHarvesting", 0.5),
    Category("cottonVehicles", 1),
    Category("cultivators", 0.5),
    Category("cutterTrailers", 0.25),
    Category("cutters", 0.5),
    Category("discHarrows", 0.5),
    Category("dollys", 0.25),
    Category("fertilizerSpreaders", 0.5),
    Category("forageHarvesterCutters", 0.25),
    Category("forageHarvesters", 1),
    Category("forklifts", 0.5),
    Category("frontLoaderTools", 0.25),
    Category("frontLoaderVehicles", 1),
    Category("frontLoaders", 0.25),
    Category("grapeTools", 0.5),
    Category("grapeVehicles", 1),
    Category("grasslandCare", 0.5),
    Category("harvesters", 2),
    Category("leveler", 0.25),
    Category("loaderWagons", 0.5),
    Category("lowloaders", 0.5),
    Category("manureSpreaders", 0.5),
    Category("misc", 0.5),
    Category("miscVehicles", 1),
    Category("mowerVehicles", 1),
    Category("mowers", 0.5),
    Category("mulchers", 0.5),
    Category("oliveVehicles", 1),
    Category("planters", 0.5),
    Category("plows", 0.5),
    Category("potatoHarvesting", 0.5),
    Category("potatoVehicles", 1),
    Category("powerHarrows", 0.5),
    Category("rollers", 0.25),
    Category("seeders", 0.5),
    Category("silocompaction", 0.25),
    Category("skidSteerTools", 0.25),
    Category("skidSteerVehicles", 1),
    Category("slurryTanks", 0.5),
    Category("slurryVehicles", 1),
    Category("spaders", 0.5),
    Category("sprayerVehicles", 1),
    Category("sprayers", 0.5),
    Category("stonePickers", 0.5),
    Category("subsoilers", 0.5),
    Category("sugarCaneHarvesting", 0.5),
    Category("sugarcaneVehicles", 1),
    Category("tedders", 0.5),
    Category("teleLoaderTools", 0.25),
    Category("teleLoaderVehicles", 1),
    Category("tractorsL", 1.5),
    Category("tractorsM", 1.5),
    Category("tractorsS", 1.5),
    Category("trailers", 0.5),
    Category("trucks", 0.5),
    Category("vegetableTools", 0.5),
    Category("vegetableVehicles", 1),
    Category("weeders", 0.5),
    Category("weights", 0.25),
    Category("wheelLoaderTools", 0.25),
    Category("wheelLoaderVehicles", 1),
    Category("windrowers", 0.5),
    Category("winterEquipment", 0.51),
    Category("wood", 0.5),
    Category("woodHarvesting", 0.5),
]


def is_valid_category(category: str) -> bool:
    # Some vehicles have multiple categories.
    # If just any of them are valid, we consider the vehicle valid.
    for cat in category.split():
        for valid_cat in CATEGORIES:
            if cat.lower() == valid_cat.category_name_lower:
                return True

    return False


def convert_category(category: str) -> str:
    """
    Takes a category and returns the proper capitalization of it

    This is required since some mods write e.g. PLANTERS instead of planters,
    or MISC instead of misc

    Keyword arguments:
    category (str) -- The category to convert

    """

    for cat in CATEGORIES:
        if cat.category_name_lower == category.lower():
            return cat.category_name

    return None


class YearSource(StrEnum):
    NONE = "none"
    GUESSTIMATE = "guesstimate"
    SOURCED = "sourced"
    INHERITED = "inherited"

class ModZipFile:
    """
    Class to hold information about a mod zip file

    Keyword arguments:
    mod_name (str) -- The mod name
    mod_id (int) -- The mod ID
    version (str) -- The mod version (like 1.0.1.0)
    rating (float) -- The mod rating
    above_rating_cnt (bool) -- Whether or not the mod's rating count is above MIN_RATING_CNT
    is_vehicle_mod (bool) -- Whether or not the mod is a vehicle mod, or e.g. a Map mod

    """

    def __init__(
        self,
        mod_name: str,
        mod_id: int,
        version: str,
        rating: float,
        above_rating_cnt: bool,
        is_vehicle_mod: bool,
    ):
        self.mod_name = mod_name
        self.mod_id = mod_id
        self.version = version
        self.version_str = self.version.replace(".", "_")
        self.rating = rating
        self.rating_str = str(self.rating).replace(".", "_")
        self.above_rating_cnt = above_rating_cnt
        self.is_vehicle_mod = is_vehicle_mod
        is_vehicle_mod_str = str(int(self.is_vehicle_mod))
        above_rating_cnt_str = str(int(self.above_rating_cnt))
        self.file_name = (f"{self.mod_name}_{self.mod_id}_{self.version_str}_{self.rating_str}_"
                          f"{above_rating_cnt_str}_{is_vehicle_mod_str}.zip")

    @classmethod
    def from_file(cls, file_name: str):
        """
        Parses filename and generates a ModZipFile from the content of it

        Keyword arguments:
        file_name (str) -- The file name to generate ModZipFile from, e.g.
                           FS22_FrontierMap_272092_1_0_1_1_4_1_1_0.zip

        """
        result = re.search(r"/(?P<mod_name>\w+)_"
                           r"(?P<mod_id>\d{6,})_"
                           r"(?P<version_str>\d_\d_\d_\d)_"
                           r"(?P<rating_str>\d_\d)_"
                           r"(?P<above_rating_cnt>\d+)_"
                           r"(?P<is_vehicle_mod_str>\d)", file_name)
        if result is not None:
            version = result["version_str"].replace("_", ".")
            rating = result["rating_str"].replace("_", ".")

            return cls(result["mod_name"], int(result["mod_id"]), version, float(rating),
                       bool(int(result["above_rating_cnt"])),
                       bool(int(result["is_vehicle_mod_str"])))

        return None

    def __str__(self):
        return self.file_name

    def __eq__(self, other):
        return self.file_name == other.file_name


class ModInfo:
    """
    Class to hold information about a mod

    Keyword arguments:
    id (int) -- The mod ID
    name (str) -- The mod name
    rating (float) -- The mod rating
    above_rating_cnt (bool) -- Whether or not the mod's rating count is above MIN_RATING_CNT
    is_vehicle_mod (bool) -- Whether or not the mod is a vehicle mod, or e.g. a Map mod

    """

    def __init__(
        self,
        id: int,
        name: str,
        version: str,
        rating: float,
        above_rating_cnt: int,
        is_vehicle_mod: bool,
    ):
        self.id = id
        self.name = name
        self.version = version
        self.rating = rating
        self.above_rating_cnt = above_rating_cnt
        self.is_vehicle_mod = is_vehicle_mod
        self.url = (f"https://www.farming-simulator.com/mod.php?lang=en&country=us&"
                    f"mod_id={id}&title=fs2022")

    @classmethod
    def from_mod_zip_file(cls, mod_zip_file: ModZipFile):
        """
        Parses ModZipFile object and generates a ModInfo from the content of it

        Keyword arguments:
        mod_zip_file (ModZipFile) -- The ModZipFile to generate ModInfo from

        """
        return cls(mod_zip_file.mod_id, mod_zip_file.mod_name, mod_zip_file.version,
                   mod_zip_file.rating, mod_zip_file.above_rating_cnt, mod_zip_file.is_vehicle_mod)

    def __str__(self):
        return (f"ModInfo({self.id}, {self.name}, {self.version}, {self.rating}, "
                f"{self.above_rating_cnt}, {self.is_vehicle_mod})")

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
        )


class Vehicle:
    """
    Class to hold Vehicle data

    Keyword arguments:
    category (str)         -- The category of the vehicle, e.g. "planters" or "tractorsL"
    brand (str)            -- The brand of the vehicle, e.g. "FENDT" or "NEWHOLLAND"
    name (str)             -- The name of the vehicle, e.g. "MLT 840-145 PS+" or "8R Series"
    raw_xml_filename (str) -- The raw XML filename used by the game.
    year (str)             -- The year of the vehicle as a string, e.g. "1992" or "2001".
                              Defaults to YEAR_TBD if not set.

    """

    def __init__(
        self,
        category: str,
        brand: str,
        name: str,
        raw_xml_filename: str,
        year: str = YEAR_TBD,
        mod_info: ModInfo = None,
        year_source: YearSource = YearSource.NONE,
    ):
        self.category = category
        self.brand = brand
        self.name = name
        self.raw_xml_filename = raw_xml_filename
        self.year = year
        self.mod_info = mod_info
        self.year_source = year_source

    def __str__(self):
        return (f"Vehicle({self.category}, {self.brand}, {self.name}, {self.raw_xml_filename}, "
                f"{self.year}, {self.mod_info})")

    def __lt__(self, other):
        if self.category < other.category:
            return True
        elif self.category == other.category:
            if self.brand < other.brand:
                return True
            elif self.brand == other.brand:
                if self.name < other.name:
                    return True
                elif self.name == other.name:
                    return self.raw_xml_filename < other.raw_xml_filename

        return False

    def __eq__(self, other):
        return (
            self.category == other.category
            and self.brand == other.brand
            and self.raw_xml_filename == other.raw_xml_filename
            and self.mod_info == other.mod_info
        )


def parse_xmlfile(input_dir: Path, path: Path, is_mod: bool = False, mod_dir: Path = None) -> Vehicle:
    """
    Parses an XML file containing a single vehicle and returns it as a
    Vehicle object

    Keyword arguments:
    input_dir (Path) -- The root directory of the vehicle
    path (Path)      -- The path of the file to parse
    is_mod (bool)    -- Whether or not this is a mod
    mod_dir (Path)   -- Root directory of the mod

    """
    try:
        root = ET.parse(str(path)).getroot()
    except:
        print(f"Failed to parse {str(path)}")
        return None

    if root.tag == "vehicle":
        store_data = root.find("storeData")
        if store_data is not None:
            show_in_store = store_data.find("showInStore")
            year_str = YEAR_TBD

            # Don"t consider hidden vehicles
            hidden = show_in_store is not None and show_in_store.text == "false"
            if hidden:
                return None

            name = store_data.find("name")
            brand = store_data.find("brand")
            category = store_data.find("category")
            mod_info = None
            raw_xml_filename = ""

            # Some vehicles have the name under a subelement for each language - Grab the english
            # name
            if name is not None:
                en_name = name.find("en")
                if en_name is not None and en_name.text is not None:
                    name.text = en_name.text

            if (
                name is None
                or name.text is None
                or brand is None
                or brand.text is None
                or category is None
                or category.text is None
            ):
                return None

            name_str = name.text.strip()
            brand_str = brand.text.strip().upper()  # brands should always be upper case
            category_str = category.text.strip()
            if not is_valid_category(category_str):
                print((f"Invalid category from {str(mod_dir)} for "
                       f"{brand_str}:{name_str}: {category_str}"))
                return None

            category_str = convert_category(category_str.split()[0])

            if is_mod:
                # If the mod declares a year itself, we use that
                mod_year = store_data.find("year")
                if mod_year is not None:
                    year_str = mod_year.text.strip()
                    print(f"Mod has year: {year_str}")

                mod_file_name = ModZipFile.from_file(str(mod_dir))
                if mod_file_name is None:
                    print(f"Invalid directory name: {str(mod_dir)}")
                    return None

                mod_info = ModInfo.from_mod_zip_file(mod_file_name)

                # The rawXMLFilename in the game for mods are always the path of the XML file
                # relative to the customEnvironment (which is the mod directory name, e.g.
                # FS22_Vehicle_Years)
                # raw_xml_filename is based on the file path minus the directory it is found to
                # get the path relative to the mod
                raw_xml_filename = path.as_posix().replace(str(mod_dir) + "/", "")
            else:
                # The rawXMLFilename in the game for non-mods are always
                # $data/vehicles/{brand}/{name}/{xmlName}
                # so modify the XML filepath here to match that
                raw_xml_filename = path.as_posix().replace(input_dir.as_posix(), "$data/vehicles")

            return Vehicle(category_str, brand_str, name_str, raw_xml_filename, year_str,
                           mod_info=mod_info)

    return None


def parse_mod_zip_file(zipfile: Path, existing_data: list[Vehicle] = None) -> list[Vehicle]:
    """
    Parses a zipfile of a mod, extracting it and parsing all the .xml files within it, and returns
    the list of vehicles within the mod.

    Keyword arguments:
    zipfile (Path) -- The path of the zip'ed mod
    """
    vehicles = []

    # If we have existing data, then we do not bother unzipping the file if the mod with the same
    # version or lower is already parsed
    if existing_data is not None:
        mod_zip_file = ModZipFile.from_file(str(zipfile))

        for v in existing_data:
            if v.mod_info.id == mod_zip_file.mod_id and v.mod_info.version >= mod_zip_file.version:
                print(f"Skipping {str(zipfile)}")
                return

    # Extract the mod into a new directory with the zip file name
    print(f"Handling {str(zipfile)}")
    extract_folder_path = zipfile.parent.joinpath(zipfile.stem)
    extract_mod_zip(zipfile, extract_folder_path)

    for xmlfile in list(extract_folder_path.glob("**/*.xml")):
        vehicle = parse_xmlfile(zipfile.parent, xmlfile, True, extract_folder_path)
        if vehicle is not None:
            vehicles.append(vehicle)

    shutil.rmtree(extract_folder_path)

    return vehicles


def parse_all_vehicles(vehicles_dir: Path, existing_data: list[Vehicle] = None,
                       is_mods: bool = False) -> list[Vehicle]:
    """
    Parses an directory containing XML files for vehicle and returns it as a
    list of Vehicle object

    Keyword arguments:
    vehicles_dir (Path) -- The path of the directory to parse

    """
    vehicles = []

    if is_mods:
        zip_file_list = list(vehicles_dir.glob("*.zip"))

        with Pool() as pool:
            vehicles = []

            result = pool.starmap(parse_mod_zip_file,
                                  [(zip_file, existing_data) for zip_file in zip_file_list])

            if result is not None:
                # Since the mod parser returns a list of lists of vehicles,
                # we must flatten the list of lists

                for sublist in result:
                    if sublist is not None:
                        vehicles = vehicles + [item for item in sublist]

            # Remove duplicates in case that the same mod exist multiple times in the input data
            # This is necessary due to the parallel computation above
            res = []
            [res.append(x) for x in vehicles if x not in res]
            vehicles = res
    else:
        for xmlfile in list(vehicles_dir.glob("**/*.xml")):
            vehicle = parse_xmlfile(vehicles_dir, xmlfile, is_mods)
            if vehicle is not None:
                vehicles.append(vehicle)

    return vehicles


def parse_existing_data(path: Path) -> list[Vehicle]:
    """
    Parses an file containing an XML file containing existing data previously
    output by this program

    Keyword arguments:
    path (Path) -- The path of the file to parse

    """
    existing_data = []

    if path.exists():
        print(f"Parsing existing data from {str(path.absolute())}")
        existing_xml = ET.parse(str(path))

        if existing_xml is not None:
            existing_root = existing_xml.getroot()

            if existing_root.tag == "vehicles":
                for cat in existing_root:
                    for brand in cat:
                        for data in brand:
                            name = data.find("name")
                            year = data.find("year")
                            mod_id = data.find("mod_id")
                            raw_xml_filename = data.find("rawXMLFilename")

                            if mod_id is not None:
                                mod_name = data.find("mod_name")
                                mod_version = data.find("mod_version")
                                rating = data.find("rating")
                                above_rating_cnt = data.find("above_rating_cnt")
                                is_vehicle_mod = data.find("is_vehicle_mod")

                                vehicle = Vehicle(
                                    cat.tag,
                                    brand.tag,
                                    name.text,
                                    raw_xml_filename.text,
                                    year=year.text,
                                    mod_info=ModInfo(int(mod_id.text),
                                                     mod_name.text,
                                                     mod_version.text,
                                                     float(rating.text),
                                                     above_rating_cnt.text == str(True),
                                                     is_vehicle_mod.text == str(True))
                                )
                            else:
                                vehicle = Vehicle(cat.tag, brand.tag, name.text,
                                                  raw_xml_filename.text, year=year.text)

                            existing_data.append(vehicle)
        print(f"Found {len(existing_data)} existing vehicles")

    return existing_data


def find_existing_vehicle(existing_data: list[Vehicle], vehicle: Vehicle) -> Vehicle:
    """
    Find existing Vehicle in a list of Vehicles by value

    Keyword arguments:
    existing_data (list[Vehicle]) -- The list of Vehicles
    vehicle (Vehicle)             -- The Vehicle to find

    """
    for v in existing_data:
        if v == vehicle:
            return v

    return None


def store_results(vehicles: list[Vehicle], data_path: Path = None) -> bool:
    """
    Stores the results of parsing FS22 game data and any existing data if
    the data_path is set, otherwise just prints the resulting data

    Keyword arguments:
    vehicles (list[Vehicle]) -- The list of Vehicle objects parsed from FS22 game data
    data_path (Path)         -- The path of the output file (if supplied). This will be used to
                                avoid overwriting any existing data while still adding new data

    """
    root_elem = ET.Element("vehicles")
    existing_data = None
    category = ""
    brand = ""

    if data_path is not None:
        existing_data = parse_existing_data(data_path)

        for vehicle in vehicles:
            for existing_vehicle in existing_data:

                if vehicle == existing_vehicle:
                    vehicle.year = existing_vehicle.year
                    vehicle.year_source = existing_vehicle.year_source
                elif (vehicle.year == YEAR_TBD and
                      existing_vehicle.year != YEAR_TBD and
                      vehicle.name == existing_vehicle.name):
                    # If the vehicle does not have a year and there's a vehicle with a matching
                    # name, we inherit the year.
                    vehicle.year = existing_vehicle.year
                    vehicle.year_source = YearSource.INHERITED

        for existing_vehicle in existing_data:
            if existing_vehicle not in vehicles:
                vehicles.append(existing_vehicle)
    vehicles.sort()  # sort by category first, then brand, then name

    # Generate XML and if the vehicle exists in the existing data, then use the
    # year from existing data to avoid overriding it with YEAR_TBD
    for vehicle in vehicles:
        if category != vehicle.category:
            category = vehicle.category
            cat_elem = ET.SubElement(root_elem, category)

        if brand != vehicle.brand:
            brand = vehicle.brand
            brand_elem = ET.SubElement(cat_elem, brand)

        vehicle_elem = ET.SubElement(brand_elem, "vehicle")

        name_elem = ET.SubElement(vehicle_elem, "name")
        name_elem.text = vehicle.name

        # year_text = vehicle.year
        # if existing_data:
        #     existing_vehicle = find_existing_vehicle(existing_data, vehicle)

        #     if existing_vehicle is not None and year_text == YEAR_TBD:
        #         year_text = existing_vehicle.year

        xmlfile_elem = ET.SubElement(vehicle_elem, "rawXMLFilename")
        xmlfile_elem.text = vehicle.raw_xml_filename

        year_source_elem = ET.SubElement(vehicle_elem, "year_source")
        year_source_elem.text = vehicle.year_source

        year_elem = ET.SubElement(vehicle_elem, "year")
        year_elem.text = vehicle.year

        if vehicle.mod_info is not None:
            id_elem = ET.SubElement(vehicle_elem, "mod_id")
            id_elem.text = str(vehicle.mod_info.id)

            mod_name_elem = ET.SubElement(vehicle_elem, "mod_name")
            mod_name_elem.text = vehicle.mod_info.name

            mod_version_elem = ET.SubElement(vehicle_elem, "mod_version")
            mod_version_elem.text = vehicle.mod_info.version

            url_elem = ET.SubElement(vehicle_elem, "mod_url")
            url_elem.text = CDATA_START_MARKER + vehicle.mod_info.url + CDATA_END_MARKER

            rating_elem = ET.SubElement(vehicle_elem, "rating")
            rating_elem.text = str(vehicle.mod_info.rating)

            above_rating_cnt_elem = ET.SubElement(vehicle_elem, "above_rating_cnt")
            above_rating_cnt_elem.text = str(vehicle.mod_info.above_rating_cnt)

            is_vehicle_mod_elem = ET.SubElement(vehicle_elem, "is_vehicle_mod")
            is_vehicle_mod_elem.text = str(vehicle.mod_info.is_vehicle_mod)

    print("Total: " + str(len(vehicles)))

    elem_tree = ET.ElementTree(root_elem)
    ET.indent(elem_tree, space="\t")
    if data_path is not None:
        # Since xml.etree.ElementTree does not support CDATA we simply convert the XML to a string and
        # store it ourselves, all while replacing our marker with proper CDATA tags
        xml_str = ET.tostring(root_elem, encoding='utf-8').decode('utf-8')
        xml_str = """<?xml version="1.0" encoding="utf-8" standalone="no" ?>\n""" + xml_str
        xml_str = xml_str.replace(CDATA_START_MARKER, CDATA_START_REPLACE)
        xml_str = xml_str.replace(CDATA_END_MARKER, CDATA_END_REPLACE)
        xml_str = xml_str.replace(XML_AMP, XML_AMP_REPLACE)

        with data_path.open("w") as f:
            f.write(xml_str)

        print("Saved file to " + str(data_path.absolute()))
    else:
        for vehicle in vehicles:
            print(str(vehicle))

    return True


def download_mod_zip(zip_file_path: Path, zip_url: str, zip_dir_path: Path):
    zip_file_path_abs = str(zip_file_path.absolute())

    # Ensure that the directory to store the zips in exist
    if not zip_dir_path.is_dir():
        zip_dir_path.mkdir()

    # Only down zip file if it doesn't already exist
    if not zip_file_path.is_file():
        print(f"Downloading {zip_url}")
        #  Add "Referer" to avoid a 403 from server
        req = urllib.request.Request(zip_url)
        req.add_header("Referer", "https://www.farming-simulator.com/")

        with urllib.request.urlopen(req) as zip_file_req:
            with open(zip_file_path_abs, "wb") as out_file:
                out_file.write(zip_file_req.read())
    else:
        print(f"{zip_file_path_abs} already exist")


def extract_mod_zip(zip_file_path: Path, output_path: Path):
    with zipfile.ZipFile(zip_file_path, "r") as zip_file:
        zip_file.extractall(output_path)


def parse_and_download_mod(mod_id: str, data_path: Path):
    # List of categories to ignore
    nonvehicle_cats: Final[list[str]] = [
        "Map",
        "Gameplay",
        "Bigbag Pallets",
        "Bigbags",
        "Pallets",
        "Sheds",
        "Silos",
        "Container",
        "Farmhouses",
        "Factories",
        "Selling Points",
        "Greenhouses",
        "Generators",
        "Animal Pens",
        "Decoration",
        "Prefab",
    ]

    mod_url = (
        f"https://www.farming-simulator.com/mod.php?lang=en&country=us&mod_id={mod_id}&title=fs2022"
    )

    print(f"Opening {mod_url}")

    try:
        with urllib.request.urlopen(mod_url) as req:
            html = req.read().decode("utf-8")

            result = re.search(
                r"Category.*>(?P<category>\w+[ \w+]*)<.*>Author<.*"
                r"Version.*>(?P<version>\d\.\d\.\d\.\d)<.*>Released<.*"
                r"mod-item__rating-num float-left\">(?P<rating>\d(\.\d)?)&nbsp;"
                r"\((?P<rating_cnt>\d+).*"
                r"(?P<url>https\S+/\d+/(?P<zip_name>\w+).zip).+>DOWNLOAD<",
                html,
                re.DOTALL,
            )

            if result is not None:
                print(result.groupdict())
                category = result["category"]

                is_vehicle_mod = category not in nonvehicle_cats

                if data_path is not None:
                    above_rating_cnt = int(result["rating_cnt"]) > MIN_RATING_CNT

                    zip_name = result["zip_name"]
                    url = result["url"]
                    zip_file_name = ModZipFile(zip_name, int(mod_id), result["version"], float(
                        result["rating"]), above_rating_cnt, is_vehicle_mod)
                    zip_file_path = data_path.joinpath(zip_file_name.file_name)

                    for downloaded_mod in list(data_path.glob("*.zip")):
                        mod_file_name = ModZipFile.from_file(str(downloaded_mod))

                        if mod_file_name is not None:
                            if int(mod_id) == mod_file_name.mod_id:
                                if result["version"] != mod_file_name.version:
                                    downloaded_mod.unlink()  # Delete the file
                                    break
                                else:
                                    # Update the rating
                                    downloaded_zip_file_path = data_path.joinpath(
                                        mod_file_name.file_name)

                                    if mod_file_name != zip_file_name:
                                        print(f"Mod with id {mod_id} is already downloaded, renaming "
                                              f"from {str(downloaded_zip_file_path)} "
                                              f"to {str(zip_file_path)}")

                                        downloaded_zip_file_path.rename(zip_file_path)
                                    else:
                                        print(f"Mod with id {mod_id} is already downloaded, "
                                              "ignoring")

                                return

                    # Download the mod and store the .zip file in the zips directory
                    download_mod_zip(zip_file_path, url, data_path)
            pass
    except urllib.error.URLError as e:
        print(f"Failed to open URL with reason {e.reason}")


def download_mods(page_count: int, data_path: Path = None):
    page = 0
    mod_ids = []

    while True:
        page_url = (f"https://www.farming-simulator.com/mods.php?lang=en&country=us&title=fs2022&"
                    f"filter=latest&page={page}")

        print(f"Loading page {page}")
        with urllib.request.urlopen(page_url) as req:
            page_compare_str = f'<span class="show-for-sr">You\'re on page</span> {page + 1}'

            html = req.read().decode("utf-8")
            result = re.findall(r"mod-item\_\_img\">.*\n.*\n.+mod_id=(?P<mod_id>\d+)", html)
            if result is not None:
                mod_ids.extend(result)

            if not page_compare_str in html:
                print(f"Stopping at page {page}")
                break

        page = page + 1

        if page_count > 0 and page >= page_count:
            break

    with Pool() as pool:
        pool.starmap(parse_and_download_mod, [(mod_id, data_path) for mod_id in mod_ids])


def generate_vehicle_list_by_category(vehicles_list: list[Vehicle], data_path: Path = None):
    result = ""
    categories = {}

    for vehicle in vehicles_list:
        if vehicle.year != YEAR_TBD:
            if vehicle.category not in categories:
                categories[vehicle.category] = []

            categories[vehicle.category].append(vehicle)

    for category, vehicles in categories.items():
        result += f"{category}:\n"

        vehicles.sort(key=lambda x: x.year)
        for vehicle in vehicles:
            if vehicle.mod_info is not None:
                result += f"\t{vehicle.year}: {vehicle.brand} - {
                    vehicle.name} (Mod: {vehicle.mod_info.name}: {vehicle.mod_info.url})\n"
            else:
                result += f"\t{vehicle.year}: {vehicle.brand} - {vehicle.name}\n"

    if data_path is not None:
        with open(data_path.absolute(), "w", encoding="utf-8") as out_file:
            out_file.write(result)
    else:
        print(result)


def generate_vehicle_list_by_year(vehicles_list: list[Vehicle], data_path: Path = None):
    result = ""

    years = {}

    for vehicle in vehicles_list:
        if vehicle.year != YEAR_TBD:
            if vehicle.year not in years:
                years[vehicle.year] = []

            years[vehicle.year].append(vehicle)

    for year, vehicles in sorted(years.items()):
        result += f"{year}:\n"

        vehicles.sort(key=lambda x: x.category)
        for vehicle in vehicles:
            if vehicle.mod_info is not None:
                result += (f"\t{vehicle.category}: {vehicle.brand} - {vehicle.name} "
                           f"(Mod: {vehicle.mod_info.name}: {vehicle.mod_info.url})\n")
            else:
                result += f"\t{vehicle.category}: {vehicle.brand} - {vehicle.name}\n"

    if data_path is not None:
        with open(data_path.absolute(), "w", encoding="utf-8") as out_file:
            out_file.write(result)
    else:
        print(result)

# Even with 1 vehicle per cat per year we end up with up to 291 mods for a single decade
# We should limit number of mods per decade to a lower value
# Prioritize per category? Drivable/non-drivable vehicles?
# Add max_per_year value per category that controls how many vehicles for a category is added.
# For example setting tractorss = 1 and cuttertrailers = 0.2 should ensure that there are 5
# times as many small tractors as cutter trailers
# For each year add the max_per_year to each category and subtract by 1 per chosen vehicle so that we
# e.g. add 1 tractorss per year, but only one cuttertrailer per 5 years


def get_mod_list_by_years(vehicles_list: list[Vehicle], start_year: int, end_year: int) -> list[Vehicle]:
    """
    Returns list of up to `count` of the best vehicles between two years (inclusive),
    e.g. 1950 and 1959

    The generated list will take the priorities from CATEGORIES to control the maximum final result.
    For example if a category has a max_per_year of 0.25, then it will only have 1 added per
    4 years.

    The resulting list will not contain any duplicated mods, but each mod may contain more than 1
    vehicle.

    Teh resulting list will have a maximum length of 100.

    Keyword arguments:
    vehicles_list (list[Vehicle]) -- The list of modded Vehicles sorted by rating
    start_year (int) -- The start year
    end_year (int) -- The end year
    """
    MIN_RATING: Final[float] = 4.0
    MAX_VEHICLE_COUNT_PER_YEAR: Final[int] = 10

    # List of mods that are blocked from the bundles for various reasons, such as not being
    # agricultural, not working, useless or simply does not fit in
    BLOCKLIST: Final[str] = [
        "FS22_Z070_Mewa", "FS22_Lizard_Kart", "FS22_Lizard1959", "FS22_N015_KOS_crossplay",
        "FS22_UniaG_U601_0", "FS22_DynamicCrushableRocksPack", "FS22_Wheelbarrow", "FS22_ToolBar",
        "FS22_AdjustableDrawbars", "FS22_GolfCart_Hunter", "FS22_Selfmade_Weight",
        "FS22_JohnDeere4020Pulling", "FS22_Buehrer6105series", "FS22_buehrer6105",
        "FS22_Lizard_MountainBike", "FS22_Lizard_Mini_Buggy", "FS22_Service_Pack",
        "FS22_HolderEDII", "FS22_cattleCane", "FS22_Concrete_Object_Pack", "FS22_Ladder_Pack",
        "FS22_Frontshield", "FS22_FrontshieldTractors", "FS22_CP03_Grain_Containers",
        "FS22_LizardHotRod", "FS22_HomemadeLiquidTank", "FS22_Lizard_z105", "FS22_Z034_Osa2",
        "FS22_Tractor_Pulling_Pack", "FS22_JohnDeere_110_4x4", "FS22_homemadeTrailer",
        "FS22_JohnDeereMiniPack", "FS22_Lizard_P93S_pack", "FS22_bigBud747",
        "FS22_BigBud_s3_Large_frame", "FS22_BigBud_Pack", "FS22_Lizard_PentaLine",
        "FS22_Lizard_AT100", "FS22_LandClearingPack", "FS22_bmVolvoSnowGear",
        "FS22_hitchSupport", "FS22_LIEBHERR_902_Pack_Crossplay", "FS22_JohnDeere445",
        "FS22_Lizard_XJ", "FS22_Lizard_MJ", "FS22_Lizard_Banshee", "FS22_JohnDeereAMT600",
        "FS22_MicroTractor_and_Implements", "FS22_O400", "FS22_Farm_Trailer",
        "FS22_Monster_Truck", "FS22_yara_N_sensor_ALS", "FS22_MultiFruitHarvesterPack",
        "FS22_Lizard_Superior_End_Dump", "FS22_seeds_addon_crossplay", "FS22_Faresin_pf226",
        "FS22_ElectricFireDefense", "FS22_TLX3500_Series", "FS22_MKII_Conveyor_Pack",
        "FS22_colossusHarvester", "FS22_Lizard_Trex_600", "FS22_Lizard_SM72_SM82",
        "FS22_SelectableBaleCapacity", "FS22_TLX2022_Series", "FS22_Liftable_Bales_Pack",
        "FS22_ElectricFireDefense_crossplay", "FS22_Lizard_Kastrnka", "FS22_Autoload_Belt",
        "FS22_Micro_Tractor_Sprayer_Package", "FS22_MODULO2", "FS22_Farmall_EYM1206",
        "FS22_Mack_RD_690", "FS22_RS250", "FS22_International_S1800",
        "FS22_Meiller_Trailer_Pack", "FS22_JD4755", "FS22_AW362_Cabover", "FS22_XSITR",
        "FS22_JohnDeere_2280", "FS22_TLX1982_Special", "FS22_PickUp1988Custom",
        "FS22_Selfmade_Bucket", "FS22_Lizard_QuadTractor", "FS22_Lizard_Trold",
        "FS22_Lizard_TRA500", "FS22_Lizard_FourTrax300", "FS22_Lizard_ATC200", "FS22_Dino_4x4",
        "FS22_Lizard_QuadBear", "FS22_AGCO_GoKart", "FS22_Lizard_Cross", "FS22_BuggyKart",
        "FS22_FHLivestock", "FS22_AgrometN237", "FS22_Z437", "FS22_John_Deere_4755",
        "FS22_IMT_5360", "FS22_Lizard_Vulcano", "FS22_Lizard_Motorcycle_SideCar", "FS22_Case5100",
        "FS22_JD8350", "FS22_kroneTurboPack", "FS22_Lizard_VNB14", "FS22_simulator22_crossplay",
        "FS22_SelfMadeBucketWithForks", "FS22_FlatbedMonsterTruck", "FS22_MiniBike",
        "FS22_lizard10Series", "FS22_Steiger_Series_IV_crossplay", "FS22_agromaszAPN30",
        "FS22_114G_Series", "FS22_claasDominator", "FS22_LizardGTF26", "FS22_tancoD80",
        "FS22_CaseIH_QT_Steigers", "FS22_Fendt800_900Favorit", "FS22_Lizard863Pack",
        "FS22_annaburgerHTDPack_crossplay", "FS22_FendtVario_300", "FS22_Fendt700VarioS4",
        "FS22_FendtVarioTMS", "FS22_CaseAustoft8800", "FS22_terragator", "FS22_Agri_Loadall",
        "FS22_cutters_attacherJointControl", "FS22_Fendt700Vario", "FS22_Fendt300VarioGen3",
        "FS22_Fendt_Vario_300_crossplay", "FS22_JohnDeere6030premium",
        "FS22_JohnDeere_ExactRate_Tanks", "FS22_Lizard_T2000", "FS22_claasLexion",
        "FS22_johnDeereS790", "FS22_forageHarvesters_pipeControl", "FS22_Precision_Sprayer_Pack",
        "FS22_Hardi_Pack", "FS22_BalerHitchPack", "FS22_TORION_1177_1511", "FS22_Claas_TORION_1914",
        "FS22_Claas_Liebherr_Pack_crossplay", "FS22_SCORPION_1033", "FS22_Schaeffer4670_Weight",
        "FS22_faresin626_Euro", "FS22_ETV_216i", "FS22_Mahindra_UTV_Pack", "FS22_JohnDeere_XUV865R",
        "FS22_ITS_SynTracPro", "FS22_Liebherr_LTM1450", "FS22_Leitwolf_Pack",
        "FS22_SeedPotatoFarmVehicles", "FS22_JD_1775NT_2018", "FS22_strawHarvestPack",
        "FS22_Pro_Bale_Fork_Autoload", "FS22_DPW_1800_Autoload", "FS22_SeedersAddon",
        "FS22_ropaKeiler2_mc", "FS22_Retriever_Plus", "FS22_AutoloadPack",
        "FS22_Versatile_4WD_Pack", "FS22_JohnDeere_9R_9RT_9RX_series_2019", "FS22_series7R",
        "FS22_John_Deere_S7", "FS22_Tempo_L_16_BD", "FS22_SAC_S780H", "FS22_RiberiRS100RB",
        "FS22_MasseyFerguson_MF1840", "FS22_TLX_48ft_Tanker", "FS22_johnDeereSeries7R_Gen1",
        "FS22_T4_40_Multi_Harvester_Pack", "FS22_7R_Series_2018", "FS22_JohnDeere_6M_7R",
        "FS22_MasseyFergusonS_Series2020", "FS22_UniversalLiquidsTransport",
        "FS22_JohnDeere_8R_2020", "FS22_Collect900_Poplar_SugarCane", "FS22_CaseIH7150_RiceVersion",
        "FS22_KroneBigPack1270VC", "FS22_tardisLoader", "FS22_TLX_Phoenix_WF",
        "FS22_JohnDeere_9RX_2022", "FS22_Fendt700VarioGen7", "FS22_UltimateBalingPack",
        "FS22_Trimmer_Domaci", "FS22_JohnDeere_7R_2020", "FS22_MasseyFerguson_S_Series",
        "FS22_Claas_Krone_Baler_Pack_With_Lizard_R90", "FS22_masseyFerguson5S_2023",
        "FS22_TLX_Phoenix", "FS22_FendtKatana", "FS22_ViTix0", "FS22_Schaeffer_2630",
        "FS22_NH_PackSkidSteerForestry", "FS22_TLX2020_Series", "FS22_LizardRollerPack",
        "FS22_LizardM99", "FS22_Lizard_MegaRoll2430_multiRoller", "FS22_Kubota_Equipment",
        "FS22_Quickbale", "FS22_transportSprayer", "FS22_masseyFerguson_5S",
        "FS22_zkroneBigPack1290HPDVC", "FS22_MasseyFergusonPack", "FS22_MasseyFerguson_8S",
        "FS22_NH_T9", "FS22_FendtFavorit500CPack", "FS22_Step_Deck_Trailer_IML21",
        "FS22_lizard_rumblerFlatBed_crossplay", "FS22_PhiberDash_crossplay", "FS22_Harrows4x",
        "FS22_Smp3_0", "FS22_Claas_Lexion600_700_US_Pack", "FS22_JohnDeere_7R_2014",
        "FS22_JohnDeere_8R_2014", "FS22_kinzeMultiFruitPlanterPack", "FS22_MasseyFerguson1200_1250",
        "FS22_MTZ_1221_RAT", "FS22_NewHollandCX", "FS22_maagIDL", "FS22_WateringCans",
        "FS22_Fiat619", "FS22_4940", "FS22_AgrometT103", "FS22_albachDiamant2000",
        "FS22_Case_IH_Traction_King_Series", "FS22_FendtFavorit500_4cyl", "FS22_FendtFavoritSeries",
        "FS22_lizardWheelLoader", "FS22_CaseIH_Patriot_250_Sugarcane", "FS22_kverneland753C",
        "FS22_LKT_81TURBO", "FS22_AkpilBulwa", "FS22_FMR_Z406", "FS22_FahrAPN", "FS22_LizardPHMini",
        "FS22_LIZARD_44202", "FS22_Steyr8150", "FS22_Steiger_tst650", "FS22_fastCoupler",
        "FS22_lizardFastCouplers", "FS22_LizardM1313", "FS22_Piccin_GAHG_RO_BR_2000",
        "FS22_TWIN_SCREW_XBR2"
    ]

    # Start all categories out at 1
    accounting = [(c, 1) for c in CATEGORIES]

    result: list[Vehicle] = []

    for index, year in enumerate(range(start_year, end_year + 1)):
        for vehicle in vehicles_list:
            mod_in_list = False
            if vehicle.mod_info.rating < MIN_RATING:
                continue

            if not vehicle.mod_info.above_rating_cnt:
                continue

            if vehicle.year == YEAR_TBD:
                continue

            if vehicle.mod_info.is_vehicle_mod is False:
                continue

            if int(vehicle.year) != year:
                continue

            if vehicle.mod_info.name in BLOCKLIST:
                continue

            # Prevent duplicates
            for v in result:
                if v.mod_info == vehicle.mod_info:
                    mod_in_list = True
                    break

                if v.name == vehicle.name:
                    mod_in_list = True
                    break

            if mod_in_list:
                continue

            for idx, (cat, val) in enumerate(accounting):
                if vehicle.category == cat.category_name:
                    if val >= 1:
                        result.append(vehicle)
                        accounting[idx] = (cat, val - 1)
                    # else no-op

            if len(result) >= (index + 1) * MAX_VEHICLE_COUNT_PER_YEAR:
                break

        # Add one for each category
        for idx, (cat, val) in enumerate(accounting):
            accounting[idx] = (cat, val + cat.max_per_year)

    return result


def generate_mod_lists_by_decades(vehicles_list: list[Vehicle], data_path: Path):
    """
    Generate list of the best vehicles from each decade from 1950 and newer, as well a pre-1950
    vehicles

    Keyword arguments:
    vehicles_list (list[Vehicle]) -- The list of modded Vehicles
    data_path (Path)              -- The path of the output directory

    """
    LIZARD_COEFFICIENT: Final[float] = 0.95
    LIZARD_BRANDS: Final[list[str]] = [
        "LIZARD", "LIZARDFORS", "LIZARDLOGISTICS", "LIZARDMOTORS", "LIZARD2", "LIZARDFARMING",
        "LIZARDINTENSE", "LIZARDFRONTLOADER", "LIZARDLAWNCARE", "LIZARDTRUCKS", "LIZARDFORESTRY"]

    BUNDLE_LISTS: Final[list[(str, int, int)]] = [
        ("pre_1950", 0, 1949),
        ("1950s", 1950, 1959),
        ("1960s", 1960, 1969),
        ("1970s", 1970, 1979),
        ("1980s", 1980, 1989),
        ("1990s", 1990, 1999),
        ("2000s", 2000, 2009),
        ("2010s", 2010, 2019),
        ("2020s", 2020, 2030),
    ]

    megabundle = []

    for vehicle in vehicles_list:
        # We apply a coefficient to Lizard brands to prefer non-Lizard brands
        if vehicle.brand in LIZARD_BRANDS:
            vehicle.mod_info.rating = vehicle.mod_info.rating * LIZARD_COEFFICIENT

    sorted_vehicles_list: FINAL[list[Vehicle]] = sorted(
        vehicles_list, key=lambda x: (x.mod_info.rating, x.mod_info.name), reverse=True)

    for name, start, end in BUNDLE_LISTS:
        vehicles: Final[list[Vehicle]] = get_mod_list_by_years(sorted_vehicles_list, start, end)

        result: str = ""

        for vehicle in vehicles:
            # Add the vehicle name and URL to the result aligned at 50 characters
            result += (f"{vehicle.mod_info.name: <50} ({vehicle.year: <6} "
                       f"{vehicle.category: <25} {vehicle.brand: <20} {vehicle.name: <45} "
                       f"{vehicle.mod_info.url})\n")

            if data_path is not None:
                with open(data_path.joinpath(name + ".txt").absolute(), "w", encoding="utf-8") as out_file:
                    out_file.write(result)
            else:
                print(result)

        megabundle = megabundle + vehicles

    if data_path is not None:
        result: str = ""

        for vehicle in megabundle:
            # Add the vehicle name and URL to the result aligned at 50 characters
            result += (f"{vehicle.mod_info.name: <50} ({vehicle.year: <6} "
                       f"{vehicle.category: <25} {vehicle.brand: <20} {vehicle.name: <45} "
                       f"{vehicle.mod_info.url})\n")

            if data_path is not None:
                with open(data_path.joinpath("megabundle.txt").absolute(), "w", encoding="utf-8") as out_file:
                    out_file.write(result)
            else:
                print(result)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Arguments for the year generator")

    subparsers = parser.add_subparsers(dest="subparser_name", help="sub-command help")

    # Basegame parsers
    basegame_parser = subparsers.add_parser(
        "basegame",
        help="Arguments for the basegame commands used to extract data from a FS22 installation",
    )
    basegame_parser.add_argument(
        "input_dir",
        type=str,
        help="The directory of the FS22 vehicles directory. Typically something like "
             "<user_specific>/Farming Simulator 22/data/vehicles/",
    )
    basegame_parser.add_argument(
        "--output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Mods parsers
    mods_parser = subparsers.add_parser(
        "mods",
        help="Arguments for the mods commands used to download and extract data from modhub",
    )

    mod_subparsers = mods_parser.add_subparsers(dest="mod_subparser_name", help="sub-command help")

    # Download Mods parsers
    mod_download_parser = mod_subparsers.add_parser(
        "download",
        help="Arguments for the mods commands used to download and extract data from modhub",
    )
    mod_download_parser.add_argument(
        "--page_count",
        type=int,
        default=0,
        help="The number of pages to process and download from. "
             "The default value of 0 indicates all. ",
    )
    mod_download_parser.add_argument(
        "--output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Generate XML Mods parsers
    mod_xml_parser = mod_subparsers.add_parser(
        "xml",
        help="Arguments for the mods commands used to generate XML file from downloaded mods",
    )
    mod_xml_parser.add_argument(
        "mod_input_dir", type=str, help="The directory of downloaded mods directory"
    )
    mod_xml_parser.add_argument(
        "--mod_output_dir",
        required=False,
        help="The directory of where the outputting XML file will be placed. "
             "May be omitted to do a dry run",
    )

    # Generate vehicle lists parsers
    analyze_parser = subparsers.add_parser(
        "download_list",
        help="Arguments for the download_list commands used to the text files with all vehicles",
    )

    args = parser.parse_args()

    if args.subparser_name and args.subparser_name == "basegame":
        input_path = Path(args.input_dir)
        vehicles = parse_all_vehicles(input_path)

        if args.output_dir:
            output_file_path = Path(args.output_dir + "vehicle_years.xml")
            store_results(vehicles, output_file_path)
        else:
            store_results(vehicles)

    if args.subparser_name == "mods":
        if args.mod_subparser_name == "download":
            print("page_count: " + str(args.page_count))

            if args.output_dir:
                output_file_path = Path(args.output_dir)
                download_mods(args.page_count, output_file_path)
            else:
                download_mods(args.page_count)

        if args.mod_subparser_name == "xml":
            input_path = Path(args.mod_input_dir)
            existing_data = None

            if args.mod_output_dir:
                output_file_path = Path(args.mod_output_dir + "vehicle_years_mods.xml")
                existing_data = parse_existing_data(output_file_path)

            vehicles = parse_all_vehicles(input_path, existing_data, True)

            if args.mod_output_dir:
                output_file_path = Path(args.mod_output_dir + "vehicle_years_mods.xml")
                store_results(vehicles, output_file_path)
            else:
                store_results(vehicles)

    if args.subparser_name == "download_list":
        script_dir = Path(__file__).parent
        data_dir = script_dir.joinpath("data")

        base_game_vehicles = parse_existing_data(data_dir.joinpath("vehicle_years.xml"))
        mod_vehicles = parse_existing_data(data_dir.joinpath("vehicle_years_mods.xml"))

        vehicles = base_game_vehicles + mod_vehicles

        generate_vehicle_list_by_category(vehicles, data_dir.joinpath("category_list.txt"))
        generate_vehicle_list_by_year(vehicles, data_dir.joinpath("year_list.txt"))

        generate_mod_lists_by_decades(mod_vehicles, data_dir)
