-- This file is part of FS22_Vehicle_Years.
--
-- FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- FS22_Vehicle_Years is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU General Public License along with
-- FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

VehicleYears = {}

local function vyprint(value)
    if value ~= nil then
        print("VehicleYears: " .. value)
    else
        print("VehicleYears: nil")
    end
end

local function log_dbg(...)
    if VehicleYears.debugEnabled then
        vyprint(...)
    end
end

local function log_dbg_verbose(...)
    if VehicleYears.verboseDebugEnabled then
        vyprint(...)
    end
end

-- Copied and modified from https://stackoverflow.com/a/27028488
local function dump(o)
    if type(o) == 'table' then
        local s = '{ '
        for k, v in pairs(o) do
            if type(k) ~= 'table' then
                if type(k) ~= 'number' then
                    k = '"' .. k .. '"'
                end
                s = s .. '[' .. k .. '] = ' .. tostring(v) .. ','
            end
        end
        return s .. '} '
    else
        return tostring(o)
    end
end

local function storeItemIsMod(storeItem)
    if storeItem == nil then
        return false
    end

    -- Mods like TidyShop may modify the storeItem.isMod to be false, but we can still check if the
    -- customEnvironment (the mod name) exist.
    -- customEnvironment is however also used for paid DLCs (pdlc), so we need to ensure that those
    -- are not considered mods by checking their baseDir (all paid DLCs are an a subdirectory called
    -- /pdlc/)
    return storeItem.customEnvironment ~= nil and string.find(storeItem.baseDir, "pdlc") == nil
end

---Returns the year of a vehicle based on category, brand and name
-- The Categories are values such as animalTransport and baleWrappers
-- @return Year of the vehicle if found otherwise nil
local function loadYearFromXML(storeItem)
    log_dbg("loadYearFromXML")

    local xml_file_name = ""
    local year = nil

    if storeItemIsMod(storeItem) then
        xml_file_name = xml_file_name .. storeItem.customEnvironment .. "_"
    end

    xml_file_name = xml_file_name .. storeItem.rawXMLFilename
    xml_file_name = xml_file_name:gsub("/", "_")
    xml_file_name = xml_file_name:gsub("$data", "base_data")

    local xml_file_path = VehicleYears.xmldatapath .. xml_file_name

    if fileExists(xml_file_path) then
        local xmlFile = XMLFile.load("TempXML", xml_file_path)
        if xmlFile ~= nil then
            year = xmlFile:getInt("year")

            if year ~= nil then
                log_dbg(string.format("year: %s", year))

                -- Vehicles with unknown years are stored as "TBD"
                if year == "TBD" or tonumber(year) == nil then
                    -- Treat TBD or invalid values as nil to not apply the year in-game
                    year = nil
                end
            end

            xmlFile:delete()
        else
            log_dbg("xmlFile was nil")
        end
    end

    return year
end

---Returns the year of a vehicle based on the mod's XML file

-- @return Year of the vehicle if found otherwise nil
local function loadYearFromMod(xmlFilePath)
    log_dbg("loadYearFromMod")

    local year = nil

    if xmlFilePath ~= nil then
        local xmlFile = XMLFile.load("TempXML", xmlFilePath)

        if xmlFile ~= nil then
            year = xmlFile:getString("vehicle.storeData.year")

            if year ~= nil then
                log_dbg("Year loaded from mod: " .. year)

                -- Vehicles with unknown years are stored as "TBD"
                if year == "TBD" or tonumber(year) == nil then
                    -- Treat TBD or invalid values as nil to not apply the year in-game
                    year = nil
                end
            end

            xmlFile:delete()
        end
    end

    return year
end

---Callback for loading the "year" spec
function VehicleYears.loadYear(xmlFile, customEnvironment, baseDir)
    log_dbg("VehicleYears.loadYear")

    -- This year does not seem to be used or store in any way...
    -- Keep for now in case we figure out a usecase for it
    return nil
end

---Callback for getting the "year" spec
-- This will use the storeItem to lookup the vehicle in our database
-- @return The year of the vehicle if found otherwise nil
function VehicleYears.getYear(storeItem, realItem)
    log_dbg("VehicleYears.getYear")
    log_dbg_verbose(dump(storeItem))
    log_dbg_verbose(dump(storeItem.specs))

    log_dbg(string.format("Vehicle: category %s, brand %s, name %s",
            storeItem.categoryName, storeItem.brandNameRaw, storeItem.name))

    log_dbg(storeItem.specs.year)

    -- We store the result in the storeItem - If it is non-nil it means that we
    -- have already stored it, and do not need to lookup in the XML file again
    if storeItem.specs.year == nil then
        if storeItemIsMod(storeItem) then
            -- Try to read the year from the mod XML files for the vehicle first
            storeItem.specs.year = loadYearFromMod(storeItem.xmlFilename)
        end

        if storeItem.specs.year == nil then
            storeItem.specs.year = loadYearFromXML(storeItem)
        end
    end

    -- If this is nil, it won't show up in the spec list
    return storeItem.specs.year
end


function VehicleYears:processAttributeData(storeItem, vehicle, saleItem)
    log_dbg("processAttributeData")

    local year = VehicleYears.getYear(storeItem, vehicle)

    if  year ~= nil then
        local itemElement = self.attributeItem:clone(self.attributesLayout)
        local iconElement = itemElement:getDescendantByName("icon")
        local textElement = itemElement:getDescendantByName("text")

        iconElement:applyProfile("shopListAttributeIconDate")
        iconElement:setVisible(true)
        textElement:setText(VehicleYears.getYear(storeItem, vehicle))

        self.attributesLayout:invalidateLayout()
    end
end

ShopConfigScreen.processAttributeData = Utils.appendedFunction(
    ShopConfigScreen.processAttributeData, VehicleYears.processAttributeData)

function VehicleYears:onFinishedLoading()
    log_dbg("onFinishedLoading")

    -- Find storeItem based on the XML file (easiest way to do it) to then compare years
    local shopItems = g_storeManager:getItems();
    for _, storeItem in pairs(shopItems) do
        -- Ensure that the specs are loaded before accessing the year
        StoreItemUtil.loadSpecsFromXML(storeItem)

        local year = VehicleYears.getYear(storeItem, nil)

        if year ~= nil then
            -- Convert to year for easier comparison
            year = tonumber(year)

            if year ~= nil then
                local pack_items = nil

                if year < 1950 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_PRE_1950")
                elseif year < 1960 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_1950S")
                elseif year < 1970 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_1960S")
                elseif year < 1980 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_1970S")
                elseif year < 1990 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_1980S")
                elseif year < 2000 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_1990S")
                elseif year < 2010 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_2000S")
                elseif year < 2020 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_2010S")
                elseif year < 2030 then
                    pack_items = g_storeManager:getPackItems("VY_PACK_2020S")
                end

                table.addElement(pack_items, storeItem.xmlFilename)
            end
        end
    end
end

-- We have to wait until the map has finished before we can initialize populate the packs
FSBaseMission.onFinishedLoading = Utils.appendedFunction(
    FSBaseMission.onFinishedLoading, VehicleYears.onFinishedLoading)

-- Do initialization if not already done - Ensured to only run once
if VehicleYears.modActivated == nil then
    g_gui:loadProfiles(g_currentModDirectory .. "guiProfiles.xml")

    -- Adding year as a spec allows us to show the value when browsing vehicles
    g_storeManager:addSpecType("year", "shopListAttributeIconDate", nil,
        VehicleYears.getYear, "vehicle")

    VehicleYears.modActivated = true
    VehicleYears.debugEnabled = false
    VehicleYears.verboseDebugEnabled = false
    VehicleYears.xmldatapath = g_currentModDirectory .. "data/"

    -- Create packs for each decade
    g_storeManager:addModStorePack(
        "VY_PACK_PRE_1950", "< 1950", "icons/pack_pre_1950.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_1950S", "1950s", "icons/pack_1950s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_1960S", "1960s", "icons/pack_1960s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_1970S", "1970s", "icons/pack_1970s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_1980S", "1980s", "icons/pack_1980s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_1990S", "1990s", "icons/pack_1990s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_2000S", "2000s", "icons/pack_2000s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_2010S", "2010s", "icons/pack_2010s.dds", g_currentModDirectory)
    g_storeManager:addModStorePack(
        "VY_PACK_2020S", "2020s", "icons/pack_2020s.dds", g_currentModDirectory)

    log_dbg("initialized")
end
