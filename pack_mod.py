#!/usr/bin/env python

# This file is part of FS22_Vehicle_Years.
#
# FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# FS22_Vehicle_Years is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Final
from zipfile import ZipFile
from argparse import ArgumentParser, BooleanOptionalAction

import xml.etree.ElementTree as ET

OUTPUT_FILE_NAME: Final[str] = "FS22_Vehicle_Years"


def split_database(xmlfile: Path) -> Path:
    """
    Loads an XML file and generates a file for each vehicle in it.
    The purpose of this is decrease the size of the files to the minimum (just the year) by only
    having a single value per file and embedding the lookup data in the file name itself.

    Keyword arguments:
    xmlfile (Path) -- The path of the file to read from and generate individual files for
    """

    if xmlfile.exists():
        existing_xml = ET.parse(str(xmlfile))

        if existing_xml is not None:
            existing_root = existing_xml.getroot()

            if existing_root.tag == "vehicles":
                files = []
                for cat in existing_root:
                    for brand in cat:
                        for vehicle in brand:
                            raw_xml_filename = vehicle.find("rawXMLFilename")
                            mod_name = vehicle.find("mod_name")
                            year = vehicle.find("year")

                            if year is None or raw_xml_filename is None:
                                print(f"Failed to parse vehicle {vehicle}")
                                continue

                            if year.text == "TBD":
                                print(f"Skipping vehicle with TBD year")
                                continue

                            if not year.text.isdigit():
                                print(f"Failure: Vehicle {raw_xml_filename.text} with "
                                      f"invalid year: {year.text}")

                                # Cleanup temp files
                                for file in files:
                                    file.unlink()

                                return None

                            xml_file_path = Path("data")
                            xml_file_name = ""
                            if mod_name is not None:
                                xml_file_name = xml_file_name + mod_name.text + "_"

                            xml_file_name = xml_file_name + raw_xml_filename.text.replace(
                                "/", "_").replace("$", "base_")
                            xml_file_path = xml_file_path.joinpath(xml_file_name)

                            root_elem = ET.Element("year")
                            root_elem.text = year.text

                            # Save new file
                            elem_tree = ET.ElementTree(root_elem)
                            elem_tree.write(str(xml_file_path), encoding="unicode",
                                            xml_declaration=True)

                            if xml_file_path not in files:
                                files.append(xml_file_path)

                return files

    return None

def pack_mod(output_dir: Path, update: bool):
    """
    Packs the mod into a single ZIP file that contains all the necessary files

    Keyword arguments:
    output_dir (Path) -- The path of the output directory (if supplied). The
                         ZIP file will be placed in this directory with the
                         name "FS22_vehicle_years.zip"

    """
    output_file: Final[str] = Path.joinpath(
        output_dir, OUTPUT_FILE_NAME + ("_update" if update else "") + ".zip")

    # Ensure that the directory to store the zips in exist
    if not output_dir.is_dir():
        output_dir.mkdir()

    base_year_files = split_database(Path("data").joinpath("vehicle_years.xml"))
    if base_year_files == None:
        print("Failed to minimize vehicle_years.xml")
        return

    mod_year_files = split_database(Path("data").joinpath("vehicle_years_mods.xml"))
    if mod_year_files == None:
        print("Failed to minimize vehicle_years_mods.xml")

        # Cleanup temp files
        for file in base_year_files:
            file.unlink()
        return

    with ZipFile(output_file.absolute(), "w") as z:
        z.write("vehicle_years.lua")
        z.write("modDesc.xml")
        z.write("icons/pack_pre_1950.dds")
        z.write("icons/pack_1950s.dds")
        z.write("icons/pack_1960s.dds")
        z.write("icons/pack_1970s.dds")
        z.write("icons/pack_1980s.dds")
        z.write("icons/pack_1990s.dds")
        z.write("icons/pack_2000s.dds")
        z.write("icons/pack_2010s.dds")
        z.write("icons/pack_2020s.dds")
        z.write("icons/icon_VY.dds", arcname="icon_VY.dds")
        z.write("guiProfiles.xml")

        for file in base_year_files:
            z.write(str(file))
        for file in mod_year_files:
            z.write(str(file))

    print("Mod saved as " + str(output_file.absolute()))

    # Cleanup temp files
    for file in base_year_files:
        file.unlink()

    for file in mod_year_files:
        file.unlink()


if __name__ == "__main__":
    parser = ArgumentParser(description="Arguments for the packer")
    parser.add_argument(
        "--output_dir",
        type=str,
        required=True,
        help="The directory to output the zipped mod",
    )
    parser.add_argument(
        "--update",
        action=BooleanOptionalAction,
        required=False,
        help="Whether to pack this mod as an update",
    )


    args = parser.parse_args()

    output_path = Path(args.output_dir)
    pack_mod(output_path, args.update)
